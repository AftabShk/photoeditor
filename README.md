## Afy's PhotoEditor

## Features
#### 1. Fine grained seperation of FG and BG
#### 2. Change BG to color
#### 3. Change BG to desired image

## Demo
<p align="center"><img src="./Demo/video.gif" width=100%></p><br>
